angular.module('myApp', ['ngRoute', 'ngStorage'])
	.config(function($routeProvider) {
	    $routeProvider
	    .when('/', {
	        templateUrl : 'app/views/login.html',
	        controller: 'loginCtrl'
	    })

	    .when('/user',{
		    	templateUrl: 'app/views/user.html',
		    	controller: 'userCtrl'
	    })
	    .when('/insert',{
		    	templateUrl: 'app/views/insert.html',
		    	controller: 'insertCtrl'
	    })
	    .when('/insert/:id',{
		    	templateUrl: 'app/views/insert.html',
		    	controller: 'insertCtrl'
	    })

	    .when('/show/:id',{
		    	templateUrl: 'app/views/show.html',
		    	controller: 'showCtrl'
	    })
	    .when('/show',{
		    	templateUrl: 'app/views/show.html',
		    	controller: 'showCtrl'
	    })
	    .when('/update/:id',{
		    	templateUrl: 'app/views/update.html',
		    	controller: 'updateCtrl'
	    })
	    // .when('/update',{
		   //  	templateUrl: 'app/views/update.html',
		   //  	controller: 'updateCtrl'
	    // })


	    .otherwise({redirectTo : '/'});
	    
	});